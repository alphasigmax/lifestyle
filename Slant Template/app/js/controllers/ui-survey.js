app.controller('SurveyController', ['$scope', '$state', '$http', 'QuestionService', function ($scope, $state, $http, QuestionService) {
	console.log("Inside SurveyController");

	$http.defaults.headers.post['Content-Type'] = 'application/json';

	$scope.title = "Welcome to the Survey";
	qid = 1;
	$scope.nextques = function(id) {

		QuestionService.getQuestions(qid).then(function(data, status, headers, config) {
		$scope.question = data;
			console.log("Result" +angular.toJson(data));
			var data = {
			    "user_id": 1,
			    "answer_id": $scope.radioValue,
			    "question_id": id,
			    "category_id": 1,
			    "survey_id": 1
			};
			$scope.submit(data);
			qid++;
			if(qid == 7) {
				qid--;
			}
			console.log("Qid" +qid);
			console.log("Answer" +angular.toJson($scope.radioValue));
	});
		console.log("id" +id);
	}

	$scope.previousques = function() {

		QuestionService.getQuestions(qid).then(function(data, status, headers, config) {
		console.log("from service" +angular.toJson(data));
		$scope.question = data;
			--qid;
			if(qid == 0) {
				qid++;
			}
			console.log("Qid" +qid);
			console.log("Answer" +angular.toJson($scope.radioValue));
	});
	}

	$scope.finish = function(id) {
		$scope.nextques(id);
		$scope.hide = 1;
		$scope.title = "Survey submitted successfully.";
	}

	$scope.submit = function(data) {
		console.log("Data" +angular.toJson(data));
		var URL = 'http://127.0.0.1:8080/api/survey-results';
		$http({
			method: "POST",
			url: URL,
			headers: {
				'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache-Control': 'no-cache'
			},
			data: data
		}).success(function(data, status, headers, config) {
			console.log("inside success");
		}).error(function(data, status, headers, config) {
			console.log("inside error" +angular.toJson(status));
		})
	}

}]);