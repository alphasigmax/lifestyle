var app =  angular.module('app');
app.service('QuestionService', ['$rootScope','Commonservice', '$http', function ($rootScope, Commonservice, $http) {

this.getQuestions = function(id) {
	var URL = 'http://localhost:8080/api/questions/' + id;
	return Commonservice.callServer({
	  		method: 'get',
	    	url : URL
	    });;
};
}]);