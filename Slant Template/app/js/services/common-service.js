'use strict';

/**
 * @ngdoc service
 * @name affimityApp.Commonservice
 * @description
 * # Commonservice
 * Service in the affimityApp.
 */
angular.module('app')
  .service('Commonservice', ['$http','$q',function($http,$q) {

  	 this.requestParam = {
  	 	method:'',
  	 	cache:'',
  	 	url:'',
  	 	data:''
  	 };
  	 this.callServer=function(params){
  	 	var deferred = $q.defer();
  	 	$http(params).
	    success(function(data, status, headers, config) {
	      if(status === 422){
	        deferred.reject(data, status, headers, config);
	      }else{
	        deferred.resolve(data, status, headers, config);
	      }
	    }).
	    error(function(data, status, headers, config) {
	      deferred.reject(data, status, headers, config);
	    });
	    return deferred.promise;

  	 }
  }]);