App.Services.CategoryService = ['$http', function ($http) {

     this.getQuestions = function(onSuccess, onError, categoryId) {
         $http.get('http://104.40.55.187/lifestyle/api/categories/' + categoryId).then(function (result) {
             onSuccess(result.data);
         });
     }

}]