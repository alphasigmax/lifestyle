'use strict';

var app = angular.module('app', ['ui.router', 'ngResource', 'ui.bootstrap', 'rzModule']);

app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  

  /*$stateProvider.state('login', {
        url: '/login',
        templateUrl: 'views/log-in.html',
        controller: 'LoginController'
  });*/

   $stateProvider.state('logout', {
        url: '/logout',
        controller: 'LogoutController'
   });
    $stateProvider.state('survey', {
        url: '/survey',
        templateUrl: 'views/survey.html',
        controller: 'SurveyController'
    });
    $stateProvider.state('home', {
        url: '/home',
        templateUrl: 'views/home.html',
        controller: 'HomeController'
    });
    $stateProvider.state('category', {
        url: '/category/:catid',
        templateUrl: 'views/category.html',
        controller: 'CategoryController'
    });
    
    $urlRouterProvider.otherwise('/survey');

}]);

app.controller(App.Controllers);
app.service(App.Services);
app.factory(App.Factories);
app.directive(App.Directives);

app.constant('configuration', {
  apiUrl: '/angular/public/'
});