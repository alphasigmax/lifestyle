'use strict';

App.Controllers.SurveyController = ['$scope', '$http', 'SurveyService', 'CategoryService', 'QuestionService', '$timeout', 'SurveyResultsService', '$state',
    function($scope, $http, surveyService, CategoryService, QuestionService, $timeout, SurveyResultsService, $state) {

        $scope.hideButton = false;
        $scope.hidePager = true;
        $scope.submitDone = false;

        var answer = [];
        var question =[];
        $scope.itemsPerPage = 4;
        $scope.slider = {
          value: 1,
          options: {
            showSelectionBar: true,
            showTicks: true,
            showTicksValues: false,
            floor: 0,
            ceil: 4,
            step: 1,
            hidePointerLabels: true,
            hideLimitLabels: true,
            stepsArray: [
              {value: 1, legend: 'Never'},
              {value: 2, legend: 'Sometimes'},
              {value: 3, legend: 'Most Days'},
              {value: 4, legend: 'Every Day'},
            ]
          }
        };

        $scope.showFeedback1Completed = false;
        $scope.showFeedback1 = function(){

            var indexLast = ($scope.currentPage-1) * $scope.itemsPerPage;
            console.log("currentPage:"+$scope.currentPage);
                if(!$scope.showFeedback1Completed && indexLast > 0 && indexLast < $scope.data.length 
                    && $scope.data[indexLast].category.categoryName != $scope.data[indexLast-1].category.categoryName){
                    console.log("return true");
                    $scope.hidePager = true;
                    return true;
                } 
                return false;
        }
        $scope.completeFeedback1 = function(){
            $scope.showFeedback1Completed = true;
            $scope.hidePager = false;
        }


        $scope.feedback1score = function(){

            var score = 0;
            for (var i = 0; i < $scope.data.length; i++) { 
                
                if($scope.data[i].category.categoryName === 'Play-Activity'){
                    score = score + $scope.data[i].selectedAnswer;
                }
                
            }
            if(score > 23){
                return 'High';
            } else if(score <= 23 && score >= 13){
                return "Medium";
            } else if(score<13){
                return "Low";
            }
        };
        $scope.feedback2score = function(){

            var score = 0;
            var scoreText = '';
            for (var i = 0; i < $scope.data.length; i++) { 
                
                if($scope.data[i].category.categoryName === 'Play-Feelings'){
                    score = score + $scope.data[i].selectedAnswer;
                }
            }
            if(score > 13){
                scoreText = 'High';
            } else if(score <= 13 && score >= 9){
                scoreText = "Medium";
            } else if(score<9){
                scoreText = "Low";
            }

            return $scope.feedback1score() + " / " + scoreText
        };

        $scope.startSurvey = function() {

            $scope.hideButton = true;
            $scope.hidePager = false;

            $scope.onSuccessGetQuestions = function(data) {
                $scope.data = data;
                console.log(data);
                $scope.totalItems = $scope.data.length;
                $scope.lenth = $scope.data.length / 4;
                $scope.currentPage = 1;
                $scope.maxSize = 5; //Number of pager buttons to show

                /*$timeout(function() {
                    $("div[name='questions']").each(function () {
                        $(this).show();
                    });
                }, 100);*/
            }

            $scope.onSuccessGetCategories = function(data) {
                $scope.categories = data;
                QuestionService.getAnswers($scope.onSuccessGetQuestions, $scope.onErrorGetQuestions, $scope.categories.id);

            }
            $scope.getCategories = function() {
                CategoryService.getQuestions($scope.onSuccessGetCategories, $scope.onErrorGetQuestions, 1);

            }

            $scope.init =  function() {
                $scope.getCategories();
            }

            $scope.init();

            $scope.onErrorGetQuestions = function() {

            }
        }
        $scope.getScope = function(value) {

        };

        

        $scope.submit = function(uid, fname, lname, email) {

            console.log($scope.genRandom());
            var user = {
                "login": "Test"+$scope.genRandom(),
                "firstName": fname,
                "lastName": lname,
                "email": email,
                "password": "43c68c2ca6945e2dfbdcc3429db00a43c68c2ca6945e2dfbdcc3429db00a",
                "activated": false,
                "langKey": "EN"
            };

            // Find all unanswered questions and save default answers

            for (var i = 0; i < $scope.data.length; i++) { 
                
                console.log($scope.slider.options.stepsArray.filter(function ( obj ) {return obj.value === $scope.data[i].selectedAnswer;})[0].legend);

                var newAnswer = {
                    "answerName": $scope.slider.options.stepsArray.filter(function ( obj ) {return obj.value === $scope.data[i].selectedAnswer;})[0].legend,
                    "answerClass": "normal",
                    "weight": $scope.data[i].selectedAnswer,
                    "question": $scope.data[i]
                };
                answer.push(newAnswer);
            }


            var wrapper = {
                answers: answer,
                user: user,
                sid: $scope.guid(),
                feedback1score: $scope.feedback1score(),
                feedback2score: $scope.feedback2score()
            };
            console.log(wrapper);
            $scope.onSuccessGetResults = function(data) {
            }

            $scope.onErrorGetQuestions = function() {

            }

            SurveyResultsService.submitResults($scope.onSuccessGetResults, $scope.onErrorGetQuestions, wrapper);
            $state.go('survey');
            location.reload();
        }

        $scope.genRandom = function() {
            return Math.floor((1 + Math.random()) * 0x10000)
              .toString(16)
              .substring(1);
          }

        $scope.category = function(category, questionHeading) {
            $scope.categoryTitle = category;
            $scope.questionHeading = questionHeading;
        }

        $scope.guid = function() {
          return $scope.s4() + $scope.s4() + '-' + $scope.s4() + '-' + $scope.s4() + '-' +
            $scope.s4() + '-' + $scope.s4() + $scope.s4() + $scope.s4();
        }

        $scope.s4 = function() {
          return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }

}];