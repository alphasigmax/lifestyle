'use strict';

App.Controllers.HomeController = ['$scope', '$http', 'HomeService', function($scope, $http, homeService) {
        
	$scope.viewModel = new SurveyViewModel(homeService);
        
}];

/**
 * The assumption is for each category we will have a list of questions
 */
function SurveyViewModel(homeService) {
    var c = {};
    var self =  this;
    var init =  function() {
    	console.log("Loading categories")
    	getCategories();
    }
    var onSuccessGetCategories = function(data) {
    	self.categories  = data;
        console.log("Question data "+JSON.stringify(data));

    }

    var onErrorGetCategories = function(err) {
    	console.log("Question error data "+JSON.stringify(err));
    }

    var getCategories = function() {
    	homeService.getCategories(onSuccessGetCategories, onErrorGetCategories);
    }
    init();

}
