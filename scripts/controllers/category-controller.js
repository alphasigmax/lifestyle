'use strict';

App.Controllers.CategoryController = ['$scope', '$http', '$stateParams', '$rootScope','CategoryService','SurveyResultsService', function($scope, $http, $stateParams,$rootScope, categoryService, surveyResultsService) {
        
	$scope.viewModel = new SurveyViewModel(categoryService, $stateParams.catid,$rootScope);
        
}];

/**
 * The assumption is for each category we will have a list of questions
 */
function SurveyViewModel(categoryService, categoryId, $rootScope) {
	var questions = {};
    var self =  this;
    
    console.log("Category id is "+categoryId)
    var init =  function() {
    	if(categoryId!=undefined){
    		getQuestions();
    	}
    }
    var onSuccessGetQuestions = function(data) {
        self.questions = data;
        console.log("Question data "+JSON.stringify(data));
        angular.forEach(self.questions, function(value, key){
        	console.log("Key ==> "+key +" -- Value ==> "+value)
        });
        if(self.questions[0]){
        	 self.currentQuestion = self.questions[0];
        }else{
        	self.isEmptySurvey = true;
        }
        // save questions once loaded into rootScope or create a separate session object after talking with ganesh
        $rootScope.questions = self.questions;
    }
   
    var saveAndGoToNextQuestion = function(){
    	var resultSavedEvent = function(){
    	    //after success redirect to the next question page
	    };
	    var resultFailureEvent = function(){
	    	
	    };
    	var surveyResult ={
    		"categoryId":categoryId,
    		"questionId":self.currentQuestion.id,
    		"answerId": self.choice,
    		"userId" : "User"//hard coded
    		
    	};
    	// save records to db
    	surveyResultsService.submitResults(resultSavedEvent,resultFailureEvent,surveyResult);
    }
    var onErrorGetQuestions = function(err) {
    	console.log("Question err data "+JSON.stringify(err));
    }

    var getQuestions = function() {
    	categoryService.getQuestions(onSuccessGetQuestions, onErrorGetQuestions, categoryId);
    }
    init();

}
