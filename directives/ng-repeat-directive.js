'use strict';
App.Directives.ngRepeatDirective = [function () {
    return function(scope, element, attrs) {
        if (scope.$last){
            // append bootstrap library
            var js = document.createElement("script");

            js.type = "text/javascript";
            js.src = "js/bootstrap-slider.js";
            document.body.appendChild(js);
            $("#progressbar").progressbar();

            $("#survey_container").wizard({
                afterSelect: function (event, state) {
                    $("#progressbar").progressbar("value", state.percentComplete);
                    $("#location").text("(" + state.stepsComplete + "/" + state.stepsPossible + ")");
                }
            });
        }
    };
}];